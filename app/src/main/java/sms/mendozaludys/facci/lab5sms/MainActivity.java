package sms.mendozaludys.facci.lab5sms;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.Permission;

public class MainActivity extends AppCompatActivity {

    EditText numero, mensaje;
    Button Enviar;

    private final int PERMISSION_SAVE=101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numero = (EditText) findViewById(R.id.TXTNumero);
        mensaje = (EditText) findViewById(R.id.TXTMensaje);
        Enviar = (Button) findViewById(R.id.BTNEnviar);

        Enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TienePermisoSmsSend()) {

                    if (numero.getText().toString().isEmpty()) {

                        numero.setError("ESCRIBA UN NUMERO");

                    }else if (mensaje.getText().toString().isEmpty()){
                        mensaje.setError("ESCRIBA UN MENSAJE");

                    }else {

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(numero.getText().toString(), null, mensaje.getText().toString(), pi, null);

                        Toast.makeText(MainActivity.this, "SMS ENVIADO", Toast.LENGTH_LONG).show();
                        numero.setText("");
                        mensaje.setText("");
                    }

                }else{

                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS}, PERMISSION_SAVE);

                }
            }
        });

    }

        private boolean TienePermisoSmsSend(){
            return ContextCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS)
                    == PackageManager.PERMISSION_GRANTED;

        }

    private boolean TienePermisoSmsReceive(){
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_MMS)
                == PackageManager.PERMISSION_GRANTED;

    }
    }

